package models;

public class Rectangle extends Shape {
    private int width;
    private int length;

    public Rectangle(int width, int length) {
        this.width = width;
        this.length = length;
    }

    public Rectangle(int width, int length, String color) {
        this.width = width;
        this.length = length;
        this.setColor(color);
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    @Override
    public double getArea() {
        return this.length*this.width;
    }

    @Override
    public String toString() {
        return "Rectangle [width=" + width + ", length=" + length + ", color=" + this.getColor() +"]";
    }
}
