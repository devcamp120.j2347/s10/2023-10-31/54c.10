import models.Rectangle;
import models.Triangle;

public class App {
    public static void main(String[] args) throws Exception {
        Rectangle r1 = new Rectangle(10, 5, "red");
        Rectangle r2 = new Rectangle(12, 5, "blue");

        System.out.println(r1.toString());
        System.out.println(r1.getArea());

        System.out.println(r2.toString());
        System.out.println(r2.getArea());

        Triangle t1 = new Triangle(10, 3, "red");
        Triangle t2 = new Triangle(15, 3, "yello");

        System.out.println(t1.toString());
        System.out.println(t1.getArea());

        System.out.println(t2.toString());
        System.out.println(t2.getArea());
    }
}
